import java.util.concurrent.ScheduledExecutorService;

/**
 * This class represents the Launcher of the system, to bypass JAVA 11 modules constraints.
 */
public final class Launcher {

    private ScheduledExecutorService scheduledExecutorService;
    private Launcher() {
    }

    /**
     * @param args unused
     */
    public static void main(final String[] args) {
        Viewer.main(args);
    }
}
