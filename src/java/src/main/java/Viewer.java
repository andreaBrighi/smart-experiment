import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import jssc.SerialPortException;

public final class Viewer extends Application {

    private static final int SCENE_WIDTH = 1500;
    private static final int SCENE_HEIGHT = 500;
    private static final int WINDOW_SIZE = 100;
    private static final double MAX_FONT_SIZE = 20.0;

    private SerialMonitor connection;
    HBox confirmBox;

    private final Label stateText = new Label();
    private final AnchorPane statePane = new AnchorPane();
    private final XYChart.Series<Number, Number> seriesP = new XYChart.Series<>();
    private final XYChart.Series<Number, Number> seriesS = new XYChart.Series<>();
    private final XYChart.Series<Number, Number> seriesA = new XYChart.Series<>();

    public void start(Stage primaryStage) {
        connection = new SerialMonitor(this);
        final ComboBox<String> comboBoxPorts = new ComboBox<>(FXCollections.observableArrayList(connection.detectPort()));
        comboBoxPorts.valueProperty()
                .addListener((observable, oldValue, newValue) -> {
                    System.out.println(newValue);
                    connection.close();
                    connection.start(newValue);
                });
        //define object for UI
        final FlowPane flow = new FlowPane();
        final NumberAxis xAxisTime = new NumberAxis();
        final NumberAxis xAxisTime2 = new NumberAxis();
        final NumberAxis xAxisTime3 = new NumberAxis();
        //define axis
        final NumberAxis yAxisPosition = new NumberAxis();
        final NumberAxis yAxisSpeed = new NumberAxis();
        final NumberAxis yAxisAcceleration = new NumberAxis();
        //x axis
        xAxisTime.setLabel("Time/s");
        xAxisTime.setAnimated(false);
        xAxisTime2.setLabel("Time/s");
        xAxisTime2.setAnimated(false);
        xAxisTime3.setLabel("Time/s");
        xAxisTime3.setAnimated(false);
        //y axis
        yAxisPosition.setLabel("Position");
        yAxisPosition.setAnimated(false);
        yAxisSpeed.setLabel("Speed");
        yAxisSpeed.setAnimated(false);
        yAxisAcceleration.setLabel("Acceleration");
        yAxisAcceleration.setAnimated(false);

        final LineChart<Number, Number> positionChart = new LineChart<>(xAxisTime, yAxisPosition);
        positionChart.setTitle("Realtime chart (Position/time)");
        positionChart.setAnimated(false);

        final LineChart<Number, Number> speedChart = new LineChart<>(xAxisTime2, yAxisSpeed);
        speedChart.setTitle("Realtime chart (Speed/time)");
        speedChart.setAnimated(false);

        final LineChart<Number, Number> accelerationChart = new LineChart<>(xAxisTime3, yAxisAcceleration);
        accelerationChart.setTitle("Realtime chart (Acceleration/time)");
        accelerationChart.setAnimated(false);

        //defining a series to display data

        //series for position

        seriesP.setName("Data Series Position");
        positionChart.getData().add(seriesP);
        //series for speed

        seriesS.setName("Data Series Speed");
        speedChart.getData().add(seriesS);
        //series for acceleration

        seriesA.setName("Data Series Acceleration");
        accelerationChart.getData().add(seriesA);

        //adding charts in a flowLayout
        flow.getChildren().add(positionChart);
        flow.getChildren().add(speedChart);
        flow.getChildren().add(accelerationChart);

        //state panel
        statePane.setPrefSize(400, 20);
        statePane.setStyle("-fx-background-color: red;");
        stateText.setText("State: NOT READY");
        stateText.setFont(new Font(MAX_FONT_SIZE));
        AnchorPane.setLeftAnchor(stateText, 0.0);
        AnchorPane.setRightAnchor(stateText, 0.0);
        statePane.getChildren().add(stateText);
        stateText.setAlignment(Pos.CENTER);

        //comboBox and statePane in HBox (top of the window)
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(15, 12, 15, 12));
        hBox.setSpacing(10);
        hBox.setStyle("-fx-background-color: #336699;");
        hBox.getChildren().addAll(comboBoxPorts,statePane);
        hBox.setAlignment(Pos.CENTER);

        //putting charts in VBox
        VBox vBox = new VBox();
        vBox.getChildren().addAll(flow);

        //HBox for confirm message from ARDUINO
        confirmBox = new HBox();
        Label confirm = new Label();
        Button confirmBtn = new Button();
        confirm.setText("Arduino has requested confirm, proceed? ");
        confirmBtn.setText("OK");
        confirmBox.getChildren().addAll(confirm,confirmBtn);
        confirmBox.setAlignment(Pos.CENTER);
        confirmBox.setVisible(false);

        //BorderPane properties
        BorderPane root = new BorderPane();
        BorderPane.setAlignment(hBox,Pos.CENTER);
        root.setTop(hBox);
        root.setCenter(vBox);
        BorderPane.setAlignment(confirmBox,Pos.CENTER);
        root.setBottom(confirmBox);

        Scene scene = new Scene(root, SCENE_WIDTH, SCENE_HEIGHT);

        primaryStage.setTitle("Viewer");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

        confirmBtn.setOnAction(event -> {
            try {
                connection.sendCmd("ok\0\r\n");
                confirmBox.setVisible(false);
            } catch (SerialPortException e) {
                e.printStackTrace();
            }
        });
    }

    public void setState(String state){
        Platform.runLater(() -> {
            switch (state) {
                case "s":
                    stateText.setText("State: SLEEP");
                    statePane.setStyle("-fx-background-color: grey;");
                    break;
                case "e":
                    stateText.setText("State: ACTIVE");
                    statePane.setStyle("-fx-background-color: #00bb2d;");
                    break;
                case "r":
                    stateText.setText("State: READY");
                    statePane.setStyle("-fx-background-color: orange;");
                    break;
                default:
                    stateText.setText("State: NOT READY");
                    statePane.setStyle("-fx-background-color: red;");
                    break;
            }
        });
    }

    public void addValue(double position, double speed, double acc, double time){
        Platform.runLater(() -> {
            seriesP.getData().add(new XYChart.Data<>(time,position));
            seriesS.getData().add(new XYChart.Data<>(time,speed));
            seriesA.getData().add(new XYChart.Data<>(time,acc));

            if (seriesP.getData().size() > WINDOW_SIZE)
                seriesP.getData().remove(0);

            if (seriesS.getData().size() > WINDOW_SIZE)
                seriesS.getData().remove(0);

            if (seriesA.getData().size() > WINDOW_SIZE)
                seriesA.getData().remove(0);
        });
    }

    public void clearValues(){
        Platform.runLater(() -> {
            seriesP.getData().clear();
            seriesS.getData().clear();
            seriesA.getData().clear();
        });
    }

    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
