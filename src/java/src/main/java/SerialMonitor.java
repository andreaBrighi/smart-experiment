import jssc.*;

import java.util.Arrays;
import java.util.List;

public class SerialMonitor implements SerialPortEventListener {
    private SerialPort serialPort;
    private volatile String state = "";
    private final Viewer viewer;
    private String buffer ="";

    public SerialMonitor(Viewer viewer) {
        this.viewer = viewer;
    }

    public List<String> detectPort() {
        String[] portNames = SerialPortList.getPortNames();
        return Arrays.asList(portNames);
    }

    public void start(String portName) {
        serialPort = new SerialPort(portName);
        try {
            serialPort.openPort();
            serialPort.setParams(SerialPort.BAUDRATE_115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN | SerialPort.FLOWCONTROL_RTSCTS_OUT);

            serialPort.addEventListener(this, SerialPort.MASK_RXCHAR);

        } catch (SerialPortException ex) {
            System.err.println("There are an error on writing string to port - " + ex);
        }
    }

    /**
     * This should be called when you stop using the port. This will prevent port
     * locking on platforms like Linux.
     */
    public synchronized void close() {
        try {
            if (serialPort != null) {
                serialPort.removeEventListener();
                serialPort.closePort();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Handle an event on the serial port. Read the data and print it.
     */
    @Override
    public synchronized void serialEvent(SerialPortEvent event) {
        if (event.isRXCHAR() && event.getEventValue() > 0) {
            String data;
            try {
                data = serialPort.readString(event.getEventValue());
                //System.out.print(data);
                buffer = buffer.concat(data);
                while (buffer.contains("state: ") && buffer.contains("\r\n")) {
                    int startSt = buffer.indexOf("state: ") + 7;
                    int endSt = buffer.indexOf("/");
                    String stateTmp = buffer.substring(startSt, endSt);
                    if (!stateTmp.equals(state)) {
                        this.state = stateTmp;
                        viewer.setState(state);
                        if (this.state.equals("e")) {
                            viewer.clearValues();
                        }
                    }


                    if (this.state.equals("e")) {
                        int startPs = buffer.indexOf("d: ", endSt) + 3;
                        int endPs = buffer.indexOf("/", startPs);
                        String sPosition = buffer.substring(startPs, endPs);
                        //System.out.println("Position=" + sPosition);
                        double position = converter(sPosition);
                        int startSp = buffer.indexOf("v: ", endPs) + 3;
                        int endSp = buffer.indexOf("/", startSp);
                        String sSpeed = buffer.substring(startSp, endSp);
                        //System.out.println("Speed=" + sSpeed);
                        double speed = converter(sSpeed);

                        int startAc = buffer.indexOf("a: ", endSp) + 3;
                        int endAc = buffer.indexOf("/", startAc);
                        String sAcceleration = buffer.substring(startAc, endAc);
                        //System.out.println("Acceleration=" + sAcceleration);
                        double acceleration = converter(sAcceleration);

                        int startTi = buffer.indexOf("t: ", endAc) + 3;
                        int endTi = buffer.indexOf("\r\n", startTi);
                        String sTime = buffer.substring(startTi, endTi);
                        //System.out.println("Time=" + sTime);
                        double time = converter(sTime);
                        viewer.addValue(position, speed, acceleration, time);
                    } else if (this.state.equals("r")) {
                        if (buffer.contains("confirm")) {
                            viewer.confirmBox.setVisible(true);
                        }
                    }
                    int end = buffer.indexOf("\r\n")+2;
                    buffer = buffer.substring(end);
                }
            } catch (Exception ex) {
                System.err.println("Error in receiving string from COM-port: " + ex);
            }
        }
    }

    public void sendCmd(String msg) throws SerialPortException {
        if (serialPort.isOpened()) {
            serialPort.writeString(msg);
        }
    }

    private double converter(String string) {
        double data;
        if (string.contains("-")) {
            data = Double.parseDouble(string.replace("-", ""));
            data = data * -1;
        } else {
            data = Double.parseDouble(string);
        }
        return data;
    }

}