#include "SonarImpl.h"
#include "Arduino.h"
#include "TemperatureSensor.h"

SonarImpl::SonarImpl(int echoPin, int trigPin, TemperatureSensor *sensor)
{
  this->echoPin = echoPin;
  this->trigPin = trigPin;
  this->sensor = sensor;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

double SonarImpl::getDistance()
{
  double tmp = sensor->getTemperatureInCelsius();
  digitalWrite(trigPin, LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin, LOW);

  /* receve echo */
  float tUS = pulseIn(echoPin, HIGH, 10000);
  float t = tUS / 1000.0 / 1000.0 / 2;
  tmp = (tmp + sensor->getTemperatureInCelsius()) / 2;
  /* calculate sound speed with temperature */
  float vs = 331.45 + 0.62 * tmp;
  float d = t * vs;
  return d;
}
