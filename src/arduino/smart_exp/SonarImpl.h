#ifndef __SONAR_IMPL__
#define __SONAR_IMPL__

#include "Sonar.h"
#include "TemperatureSensor.h"

class SonarImpl : public Sonar
{

public:
  SonarImpl(int echoPin, int trigPin, TemperatureSensor *sensor);
  double getDistance();

private:
  int echoPin;
  int trigPin;
  TemperatureSensor *sensor;
};

#endif
