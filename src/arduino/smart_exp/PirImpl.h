#ifndef __PIR_IMPL__
#define __PIR_IMPL__

#include "Pir.h"

class PirImpl : public Pir
{

public:
    PirImpl(int pin);
    unsigned long lastPresenceTime();
    bool hadPresence();
    bool isEnable();
    void enable();
    void disable();

protected:
    void notifyInterrupt(int pin);

private:
    int pin;
    unsigned long lastTime;
    bool active;
};

#endif
