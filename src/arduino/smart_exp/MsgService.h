#ifndef __MSG_SERVICE__
#define __MSG_SERVICE__

#include "Arduino.h"
#include "GlobalState.h"

class Msg
{
  String content;

public:
  Msg(String content)
  {
    this->content = content;
  }

  String getContent()
  {
    return content;
  }
};

class Pattern
{
public:
  virtual boolean match(const Msg &m) = 0;
};

class MsgServiceClass
{

public:
  Msg *currentMsg;
  bool msgAvailable;

  void init();

  bool isMsgAvailable();
  Msg *receiveMsg();

  bool isMsgAvailable(Pattern &pattern);

  Msg *receiveMsg(Pattern &pattern);

  void askConfirm();

  void sendMsg(GlobalState state);

  void sendMsg(double pos, double speet, double acc, int time);
};

extern MsgServiceClass MsgService;

#endif
