#include "DetectTask.h"
#include "Pir.h"
#include "Light.h"
#include "Arduino.h"
#include "MsgService.h"
#include "GlobalState.h"
#include <avr/sleep.h>
#include <avr/power.h>

#define SLEEP_TIME 5000

DetectTask ::DetectTask(Pir *pir,
                        Light *l1,
                        GlobalState *globalState)
{
    this->l1 = l1;
    this->pir = pir;
    this->globalState = globalState;
}

void DetectTask::init(int period)
{
    Task::init(period);
    this->state = ACTIVE;
    this->passTime = 0;
}

void sleep(void)
{
    Serial.flush();
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_mode();
    sleep_disable();
    power_all_enable();
}

void DetectTask::tick()
{
    if (*(this->globalState) == GlobalState::ACTIVE && pir->isEnable())
    {
        pir->disable();
    }
    if (*(this->globalState) != GlobalState::ACTIVE && !pir->isEnable())
    {
        pir->enable();
    }
    switch (this->state)
    {
    case WAIT_TO_SLEEP: /* prepare for sleep */
        if (!this->pir->hadPresence())
        {
            this->state = SLEEP;
            *(this->globalState) = GlobalState::SLEEP;
            this->l1->switchOff();
            MsgService.sendMsg(*(this->globalState));
            sleep();
        }
        else
        {
            this->state = ACTIVE;
        }
        break;
    case SLEEP: /* check if stai in sleep mode */
        if (this->pir->hadPresence())
        {
            this->state = ACTIVE;
            *(this->globalState) = READY;
            this->l1->switchOn();
            MsgService.sendMsg(*(this->globalState));
        }
        else
        {
            sleep();
        }
        break;
    case ACTIVE: /* wait for time passed */
        unsigned long actualTime = millis();
        unsigned long PirTime = this->pir->lastPresenceTime();     /* get last time that the pir get a presencs */
        PirTime = PirTime < actualTime ? actualTime - PirTime : 0; /* operation to work with overflow */
        this->passTime += this->myPeriod;                          /* update to new value of time (non effective)*/
        this->passTime = this->passTime < PirTime ? this->passTime : PirTime;
        if (this->passTime > SLEEP_TIME)
        {
            this->state = WAIT_TO_SLEEP;
        }
        else if (*(this->globalState) == GlobalState::ACTIVE)
        {
            this->passTime = 0; /* during experment always active */
        }
        break;
    }
}