#ifndef __GLOBAL_STATE__
#define __GLOBAL_STATE__

enum GlobalState
{
    SLEEP,
    READY,
    ACTIVE
};

#endif