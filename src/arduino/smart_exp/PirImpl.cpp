#include "PirImpl.h"
#include "Arduino.h"

PirImpl::PirImpl(int pin)
{
    this->pin = pin;
    pinMode(pin, INPUT);
    this->active = false;
    this->lastTime = millis();
}

void PirImpl::enable()
{
    bindInterrupt(pin);
    this->active = true;
}

void PirImpl::disable()
{
    detachInterrupt(pin);
    this->active = false;
}

unsigned long PirImpl::lastPresenceTime()
{
    noInterrupts();
    unsigned long time = this->lastTime;
    interrupts();
    this->lastTime = this->hadPresence() ? millis() : time;
    return this->lastTime;
}

bool PirImpl::hadPresence()
{
    return digitalRead(pin) == HIGH;
}

bool PirImpl::isEnable()
{
    return this->active;
}

void PirImpl::notifyInterrupt(int pin)
{
    this->lastTime = millis();
}
