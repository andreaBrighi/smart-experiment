#ifndef __POTENTIOMENTER_IMPL__
#define __POTENTIOMENTER_IMPL__

#include "Potentiometer.h"

class PotentiometerImpl : public Potentiometer
{

public:
  PotentiometerImpl(int pin);
  int getMappedValue(int min, int max, int interval);

private:
  int pin;
};

#endif
