#ifndef __BLINK_TASK__
#define __BLINK_TASK__

#include "Task.h"
#include "Light.h"

class BlinkTask : public Task
{
    Light *l;
    enum
    {
        ON,
        OFF
    } state;

public:
    BlinkTask(Light *l);
    void init(int period);
    void tick();
};

#endif
