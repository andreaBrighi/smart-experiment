#include "Light.h"
#include "MsgService.h"
#include "SmartExperimentTask.h"
#include "Arduino.h"
#include "GlobalState.h"

SmartExperimentTask::SmartExperimentTask()
{
}

/* initial settings */
void SmartExperimentTask::init(int period)
{
    Task::init(period);
    state = GlobalState::READY; /* initial state */
    l1->switchOn();
    MsgService.init();
    MsgService.sendMsg(state);
    tasks[0]->init(200);
    tasks[1]->init(100);
    tasks[2]->init(20);
}

void SmartExperimentTask::tick()
{
    for (int i = 0; i < 3; i++)
    {
        if (this->tasks[i]->updateAndCheckTime(this->myPeriod))
        {
            this->tasks[i]->tick();
        }
    }
}