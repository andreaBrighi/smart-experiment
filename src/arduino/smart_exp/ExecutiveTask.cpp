
#include "Sonar.h"
#include "ExecutiveTask.h"
#include "ServoMotor.h"
#include "MsgService.h"
#include "Arduino.h"
#include "GlobalState.h"

#define MIN_SPEED -5
#define MAX_SPEED 5
#define ORIGIN_ANGLE 90
#define MAX_ANGLE 180

ExecutiveTask::ExecutiveTask(Sonar *sonar,
                             ServoMotor *servo)
{
    this->sonar = sonar;
    this->servo = servo;
}

void ExecutiveTask::init(int period)
{
    Task::init(period);
    this->state = DISTANCE; /* initial state */
}

void ExecutiveTask::tick()
{
    double distance = this->sonar->getDistance() * 1000; /* read distance from sonar */
    if (distance != 0)                                   /* if is not null */
    {
        switch (this->state)
        {
        case DISTANCE: /* set previous distance */
            this->prevDistance = distance;
            this->state = VELOCITY;
            break;
        case VELOCITY: /* set previous speed and distance */
            this->prevSpeed = (distance - this->prevDistance) / this->myPeriod;
            this->prevDistance = distance;
            this->state = NORMAL;
            this->time = 0;
            break;
        case NORMAL: /* normal calulation of distance, speed and acceleration */
            double speed = (double)(distance - this->prevDistance) / this->myPeriod;
            double acceleration = (speed - this->prevSpeed) / this->myPeriod;
            int angle = ((speed * MAX_ANGLE) / (MAX_SPEED - MIN_SPEED)) + ORIGIN_ANGLE;
            servo->setPosition(angle);
            this->time += this->myPeriod;
            MsgService.sendMsg(distance, speed, acceleration, this->time);
            this->prevDistance = distance;
            this->prevSpeed = speed;
        }
    }
    else
    {
        this->servo->setPosition(ORIGIN_ANGLE); /* reset servo postion */
    }
}