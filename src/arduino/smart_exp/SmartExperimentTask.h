#ifndef __SMART_EXPERIMENT_TASK__
#define __SMART_EXPERIMENT_TASK__

#include "LM335.h"
#include "SonarImpl.h"
#include "ServoMotorImpl.h"
#include "ServoMotor.h"
#include "Sonar.h"
#include "TemperatureSensor.h"
#include "Potentiometer.h"
#include "PotentiometerImpl.h"
#include "Button.h"
#include "ButtonImpl.h"
#include "Light.h"
#include "Led.h"
#include "Pir.h"
#include "PirImpl.h"
#include "Task.h"
#include "ExperimentTask.h"
#include "CheckTask.h"
#include "DetectTask.h"
#include "MsgService.h"
#include "GlobalState.h"
#include "Arduino.h"

// define const value
#define vcc 5.0
#define tempPin A1
#define potentiometerPin A0
#define pirPin 2
#define servoPin 6
#define echoPin 7
#define trigPin 8
#define BstartPin 10
#define BstopPin 9
#define L1Pin 11
#define L2Pin 12

class SmartExperimentTask : public Task
{
private:
    /* create  sensor in oop */
    TemperatureSensor *temperature = new LM335(tempPin, vcc);
    Sonar *sonar = new SonarImpl(echoPin, trigPin, temperature);
    ServoMotor *servo = new ServoMotorImpl(servoPin);
    Potentiometer *potentiometer = new PotentiometerImpl(potentiometerPin);
    Button *bStart = new ButtonImpl(BstartPin);
    Button *bStop = new ButtonImpl(BstopPin);
    Light *l1 = new Led(L1Pin);
    Light *l2 = new Led(L2Pin);
    Pir *pir = new PirImpl(pirPin);

    /* create tasks */
    Task *tasks[3] = {
        new DetectTask(pir, l1, &state),
        new CheckTask(sonar, bStart, bStop, l2, &state),
        new ExperimentTask(sonar, servo, potentiometer, &state)};

    GlobalState state; /* global state */

public:
    SmartExperimentTask();
    void init(int period);
    void tick();
};

#endif