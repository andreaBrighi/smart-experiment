#include "BlinkTask.h"
#include "Light.h"
#include "Arduino.h"

BlinkTask ::BlinkTask(Light *l)
{
    this->l = l;
}
/* initail state and switch on */
void BlinkTask::init(int period)
{
    Task::init(period);
    this->state = OFF;
}
/* switch le d on and off */
void BlinkTask::tick()
{
    switch (this->state)
    {
    case ON:
        this->l->switchOff();
        this->state = OFF;
        break;
    case OFF:
        this->l->switchOn();
        this->state = ON;
        break;
    }
}
