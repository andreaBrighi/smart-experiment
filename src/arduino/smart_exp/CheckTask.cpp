#include "CheckTask.h"
#include "BlinkTask.h"
#include "Sonar.h"
#include "Button.h"
#include "Light.h"
#include "Arduino.h"
#include "MsgService.h"
#include "Potentiometer.h"
#include "GlobalState.h"

#define MAX_TIME 20000
#define ERROR_TIME 2000
#define BLINK_ERROR_TIME 100
#define BLINK_CONFIRM_TIME 200

CheckTask ::CheckTask(Sonar *sonar,
                      Button *bStart,
                      Button *bStop,
                      Light *l2,
                      GlobalState *globalState)
{
    this->sonar = sonar;
    this->bStart = bStart;
    this->bStop = bStop;
    this->l2 = l2;
    this->globalState = globalState;
    this->task = new BlinkTask(this->l2);
}

void CheckTask::init(int period)
{
    Task::init(period);
    this->state = WAIT_START;
}

void CheckTask::tick()
{
    /* if slepp reset */
    if (*(this->globalState) == GlobalState::SLEEP)
    {
        this->l2->switchOff();
        this->state = WAIT_START;
    }
    switch (this->state)
    {
    case WAIT_CONFIRM: /* wait for confirm */
        if (this->task->updateAndCheckTime(this->myPeriod))
        {
            this->task->tick();
        }
        if (MsgService.isMsgAvailable())
        {
            Msg *msg = MsgService.receiveMsg();
            if (msg->getContent() == "ok")
            {
                this->state = WAIT_START;
                this->l2->switchOff();
            }
            delete msg;
        }
        break;
    case WAIT_EXPERIMENT: /* wait to end experiment */
        this->counter += this->myPeriod;
        if (this->bStop->isPressed() || this->counter >= MAX_TIME)
        {
            this->counter = 0;
            this->state = WAIT_CONFIRM;
            *(this->globalState) = READY;
            MsgService.askConfirm();
            this->task->init(BLINK_CONFIRM_TIME);
        }
        break;
    case ERROR: /* show error */
        if (this->task->updateAndCheckTime(this->myPeriod))
        {
            this->task->tick();
        }
        this->counter += this->myPeriod;
        if (this->counter >= ERROR_TIME)
        {
            this->l2->switchOff();
            this->state = WAIT_START;
        }
        break;
    case WAIT_START: /* wait for start */
        if (this->bStart->isPressed())
        {
            this->counter = 0;
            this->state = CHECK_SONAR;
        }
        break;
    case CHECK_SONAR: /* check for objects presence  */
        this->counter++;
        double distance = this->sonar->getDistance();
        if (distance != 0.0)
        {
            this->state = WAIT_EXPERIMENT;
            this->counter = 0;
            *(this->globalState) = ACTIVE;
            this->l2->switchOn();
        }
        else
        {
            if (this->counter == 10)
            {
                this->state = ERROR;
                this->counter = 0;
                this->task->init(BLINK_ERROR_TIME);
            }
        }
        break;
    }
}
