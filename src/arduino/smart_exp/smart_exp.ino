/* 
    Andrea  Brighi
    Davide Ciandrini
 */

#include "Scheduler.h"
#include "SmartExperimentTask.h"

/* create task */
Task *task = new SmartExperimentTask();

Scheduler scheduler; /* scheduler */

void setup()
{
    scheduler.init(20);
    task->init(20);
    scheduler.addTask(task);
}

void loop()
{
    scheduler.schedule();
}