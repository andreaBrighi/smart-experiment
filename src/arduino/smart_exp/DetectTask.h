#ifndef __DETECT_TASK__
#define __DETECT_TASK__

#include "Task.h"
#include "Light.h"
#include "Pir.h"
#include "GlobalState.h"

class DetectTask : public Task
{
    Light *l1;
    Pir *pir;
    enum
    {
        SLEEP,
        ACTIVE,
        WAIT_TO_SLEEP
    } state;
    GlobalState *globalState;
    unsigned long passTime;

public:
    DetectTask(Pir *pir, Light *l1, GlobalState *globalState);
    void init(int period);
    void tick();
};

#endif
