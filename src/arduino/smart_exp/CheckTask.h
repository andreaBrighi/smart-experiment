#ifndef __CHECK_TASK__
#define __CHECK_TASK__

#include "Task.h"
#include "Sonar.h"
#include "Button.h"
#include "Light.h"
#include "GlobalState.h"

class CheckTask : public Task
{
    Sonar *sonar;
    Button *bStart;
    Button *bStop;
    Light *l2;
    Task *task;
    enum
    {
        WAIT_START,
        CHECK_SONAR,
        WAIT_EXPERIMENT,
        WAIT_CONFIRM,
        ERROR
    } state;
    GlobalState *globalState;
    int counter;

public:
    CheckTask(Sonar *sonar,
              Button *bStart,
              Button *bStop,
              Light *l2,
              GlobalState *globalState);
    void init(int period);
    void tick();
};

#endif
