#ifndef __PIR__
#define __PIR__

#include "event.h"

#define MAX_PIR_LISTENERS 1

class Pir : public EventSource
{

public:
    virtual bool hadPresence() = 0;
    virtual bool isEnable() = 0;
    virtual unsigned long lastPresenceTime() = 0;
    virtual void enable() = 0;
    virtual void disable() = 0;
};

#endif