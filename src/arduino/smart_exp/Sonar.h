#ifndef __SONAR__
#define __SONAR__

class Sonar
{

public:
  virtual double getDistance() = 0;
};

#endif
