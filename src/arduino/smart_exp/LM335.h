#ifndef __LM335__
#define __LM335__

#include "TemperatureSensor.h"

class LM335 : public TemperatureSensor
{
public:
  LM335(int analogPin, double vcc);
  double getTemperatureInCelsius();

private:
  int analogPin;
  double vcc;
};

#endif
