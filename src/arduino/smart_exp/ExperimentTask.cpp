#include "ExperimentTask.h"
#include "ExecutiveTask.h"
#include "Sonar.h"
#include "Task.h"
#include "ServoMotor.h"
#include "Potentiometer.h"
#include "Arduino.h"
#include "GlobalState.h"

#define MAX_TIME 1000
#define MIN_TIME 20
#define INTERVAL 20
#define ORIGIN_ANGLE 90

ExperimentTask ::ExperimentTask(Sonar *sonar,
                                ServoMotor *servo,
                                Potentiometer *potentiometer,
                                GlobalState *globalState)
{
    this->sonar = sonar;
    this->servo = servo;
    this->potentiometer = potentiometer;
    this->globalState = globalState;
    this->task = new ExecutiveTask(this->sonar, this->servo);
}

void ExperimentTask::init(int period)
{
    Task::init(period);
    this->state = WAIT;
}

void ExperimentTask::tick()
{
    if (*(this->globalState) == GlobalState::ACTIVE)
    {
        switch (this->state)
        {
        case WAIT: /* reset counter */
            this->counter = 0;
            this->state = CALIBRATE;
            break;
        case CALIBRATE: /* calibrate sensors */
            if (this->counter < 10)
            {
                this->counter++;
            }
            else
            {
                this->state = EXPERIMENT;
                this->servo->on();
                this->servo->setPosition(ORIGIN_ANGLE);
                int value = this->potentiometer->getMappedValue(MIN_TIME, MAX_TIME, INTERVAL);
                this->task->init(value);
            }
            this->sonar->getDistance();
            break;
        case EXPERIMENT: /* do experment */
            if (this->task->updateAndCheckTime(this->myPeriod))
            {
                this->task->tick();
            }
            break;
        }
    }
    else
    {
        this->state = WAIT;
        this->servo->setPosition(ORIGIN_ANGLE);
        this->servo->off();
    }
}
