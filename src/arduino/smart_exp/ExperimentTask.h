#ifndef __EXPERIMENT_TASK__
#define __EXPERIMENT_TASK__

#include "Task.h"
#include "Sonar.h"
#include "ServoMotor.h"
#include "Potentiometer.h"
#include "GlobalState.h"

class ExperimentTask : public Task
{
    Sonar *sonar;
    ServoMotor *servo;
    Potentiometer *potentiometer;
    enum
    {
        WAIT,
        CALIBRATE,
        EXPERIMENT
    } state;
    GlobalState *globalState;
    int counter;
    Task *task;

public:
    ExperimentTask(Sonar *sonar,
                   ServoMotor *servo,
                   Potentiometer *potentiometer,
                   GlobalState *globalState);
    void init(int period);
    void tick();
};

#endif
