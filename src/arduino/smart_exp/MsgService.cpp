#include "Arduino.h"
#include "MsgService.h"
#include "GlobalState.h"

String content;

MsgServiceClass MsgService;

bool MsgServiceClass::isMsgAvailable()
{
  return msgAvailable;
}

Msg *MsgServiceClass::receiveMsg()
{
  if (msgAvailable)
  {
    Msg *msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;
  }
  else
  {
    return NULL;
  }
}

void MsgServiceClass::init()
{
  Serial.begin(115200);
  content.reserve(256);
  content = "";
  currentMsg = NULL;
  msgAvailable = false;
}

String converter(GlobalState state) /* serial value ofstate */
{
  switch (state)
  {
  case SLEEP:
    return "s";
    break;
  case ACTIVE:
    return "e";
    break;
  case READY:
    return "r";
    break;
  }
}

void MsgServiceClass::sendMsg(GlobalState state) /* state message */
{
  Serial.println("state: " + converter(state) + "/");
}

void MsgServiceClass::sendMsg(double pos, double speed, double acc, int time) /* esperiment message */
{
  Serial.println("state: " + converter(GlobalState::ACTIVE) + "/d: " + String(pos) + "/v: " + String(speed) + "/a: " + String(acc) + "/t: " + String(time));
}

void MsgServiceClass::askConfirm() /* confim message */
{
  Serial.println("state: r/confirm");
}

void serialEvent()
{
  /* reading the content */
  while (Serial.available())
  {
    char ch = (char)Serial.read();
    if (ch == '\n')
    {
      MsgService.currentMsg = new Msg(content);
      MsgService.msgAvailable = true;
    }
    else
    {
      content += ch;
    }
  }
}

bool MsgServiceClass::isMsgAvailable(Pattern &pattern)
{
  return (msgAvailable && pattern.match(*currentMsg));
}

Msg *MsgServiceClass::receiveMsg(Pattern &pattern)
{
  if (msgAvailable && pattern.match(*currentMsg))
  {
    Msg *msg = currentMsg;
    msgAvailable = false;
    currentMsg = NULL;
    content = "";
    return msg;
  }
  else
  {
    return NULL;
  }
}
