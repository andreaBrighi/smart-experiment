#include "PotentiometerImpl.h"
#include "Arduino.h"
#include <math.h>

PotentiometerImpl::PotentiometerImpl(int pin)
{
  this->pin = pin;
}

int PotentiometerImpl::getMappedValue(int min, int max, int interval)
{
  return round((analogRead(pin) / 1024.0 * (max - min) + interval / 2.0) / interval) * interval + min;
}
