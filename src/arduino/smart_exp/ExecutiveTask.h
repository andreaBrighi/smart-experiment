#ifndef __EXECUTIVE_TASK__
#define __EXECUTIVE_TASK__

#include "Sonar.h"
#include "Task.h"
#include "ServoMotor.h"
#include "Arduino.h"

class ExecutiveTask : public Task
{
private:
    Sonar *sonar;
    ServoMotor *servo;
    enum
    {
        DISTANCE,
        VELOCITY,
        NORMAL
    } state;
    double prevDistance;
    double prevSpeed;
    int time;

public:
    ExecutiveTask(Sonar *sonar,
                  ServoMotor *servo);
    void init(int period);
    void tick();
};

#endif