#ifndef __TEMPERATURE_SENSOR__
#define __TEMPERATURE_SENSOR__

class TemperatureSensor
{

public:
  virtual double getTemperatureInCelsius() = 0;
};

#endif
