#include "LM335.h"
#include "Arduino.h"

LM335::LM335(int analogPin, double vcc)
{
  this->analogPin = analogPin;
  this->vcc = vcc;
}

double
LM335::getTemperatureInCelsius()
{
  return ((analogRead(analogPin) * vcc / 1024.0) / 0.01) - 273;
}
