#ifndef __POTENTIOMENTER__
#define __POTENTIOMENTER__

class Potentiometer
{

public:
  virtual int getMappedValue(int min, int max, int interval) = 0;
};

#endif